#!/usr/bin/env ruby

require "plist"
require "set"
require "pp"

class PackageFamily
  attr_reader :name
  attr_reader :packages
  attr_reader :catalogs
  attr_reader :refcount
  
  attr :dependencies
  
  def initialize(name, package = nil)
    @name = name
    @packages = []
    @dependencies = []
    @catalogs = Set.new
    @refcount = 0
    
    if (package)
      add_package(package)
    end
  end
  
  def add_catalogs(catalogs)
    @catalogs.merge catalogs
  end
  
  def add_package(package)
    @packages << package
  end
  
  def retain
    @refcount += 1
  end
  
  def release
    @refcount -= 1
    if (@refcount < 0)
      puts "WARNING: #{self} released below zero"
    end
  end
  
  def find_version(version_to_find)
    version_digits = version_to_find.split(".")
    version_digits.count.downto(1) do |significant_digits|
      version_to_search = version_digits[0...significant_digits].join(".")
      matches = @packages.find_all { |package| package.version.start_with?(version_to_search) }
      return matches if matches.count > 0
    end
  end
  
end


class Package
  attr_reader :name
  attr_reader :version
  attr_reader :os_minimum
  attr_reader :os_maximum
  attr_reader :catalogs
  attr_reader :refcount
  
  attr_accessor :dependencies
  
  def initialize(name, version_string, catalogs, os_minimum, os_maximum)
    @name = name
    @version = version_string
    @catalogs = catalogs
    @os_minimum = os_minimum || "0.0.0"
    @os_maximum = os_maximum || "99.99.99"
    
    @dependencies = []
    
    @refcount = 0
    
  end
  
  def retain
    @refcount += 1
  end
  
  def release
    @refcount -= 1
    if (@refcount < 0)
      puts "WARNING: #{self} released below zero"
    end
  end
  
end
  
package_families = {}
dependencies = []
pkgcount = 0
Plist::parse_xml("all").each do |p|
  begin
    package = Package.new(p["name"], p["version"], Set.new(p["catalogs"]), p["minimum_os_version"], p["maximum_os_version"])
    
    # Don't include apple installer stubs
    if p["installer_type"] == "apple_update_metadata"
      next
    end
    
    depmap = Set.new

    if p["requires"]
      depmap.merge(p["requires"])
    end
    
    if p["update_for"]
      depmap.merge(p["update_for"])
    end
    
    if depmap.count > 0
      dependencies << [ package, depmap ]
    end
    
    package_families[p["name"]] ||= PackageFamily.new(p["name"])
    package_families[p["name"]].add_catalogs(Set.new(p["catalogs"]))
    package_families[p["name"]].add_package(package)
    
    pkgcount += 1
    
  rescue Exception => e
    puts e
    puts p["name"], p["version"]
  end
end

# Walk through the requirements list and update package references
dependencies.each do |package, requirements|
  requirements.each do |requirement|

    # See if there's a package named this specific thing before we go looking for digits
    # Example: LoginHook-Framework
    package_family = package_families[requirement]
    if package_family
      if ((package_family.catalogs & package.catalogs).count) == 0
        # TODO better warning
        # puts "WARNING: requirement's catalogs do not align with required item"
      end
      package_family.retain()
      package.dependencies << package_families[requirement]
      
    else
      # Nope, so start a version search
      (package_name, version) = requirement.split("-")
      family = package_families[package_name]
      
      if family.nil?
        # TODO better warning
        # puts "MISSING FAMILY: #{package_name}"
        next
      end
      
      matching_packages = package_families[package_name].find_version(version)

      if (matching_packages && matching_packages.count > 0)
        matching_packages.each do |required_package|
          if (required_package.catalogs & package.catalogs).count == 0
            # TODO better warning
            # puts "WARNING: #{package.name} requirement's catalogs do not align with required item"
          end
          required_package.retain()
          package.dependencies << required_package
          
        end
      else
        puts "NOTFOUND" # TODO something clever
      end
      
    end
    
  end
end

def version_lt(a, b)
  a_array = a.split('.').map {|e| e.to_i }
  b_array = b.split('.').map {|e| e.to_i }
  (a_array <=> b_array) < 0
end


def version_lte(a, b)
  a_array = a.split('.').map {|e| e.to_i }
  b_array = b.split('.').map {|e| e.to_i }
  (a_array <=> b_array) <= 0
end

def version_gte(a, b)
  a_array = a.split('.').map {|e| e.to_i }
  b_array = b.split('.').map {|e| e.to_i }
  (a_array <=> b_array) >= 0
end

old_count = 0
deletions = Set.new
begin
  old_count = deletions.count
  package_families.each do |family_name, family|

    to_delete = []

    family.packages.each do |current_package|

      # Is there some other package in this family that totally occludes this package?
      to_delete << current_package if family.packages.any? do |other_package|

        # Catalog coverage is complete
        current_package.catalogs.subset?(other_package.catalogs) && 

        # version is higher
        version_lt(current_package.version, other_package.version) &&
    
        # OS minimum
        version_gte(current_package.os_minimum, other_package.os_minimum) &&
    
        # OS maximum
        version_lte(current_package.os_maximum, other_package.os_maximum) &&
        
        current_package.refcount == 0
            
      end
      
    end

    to_delete.each do |deletion|
      family.packages.delete(deletion)
      deletion.dependencies.each { |dep| dep.release }
      deletion.dependencies = []
    end
    
    deletions.merge(to_delete)

  end
  
end while deletions.count > old_count

deletions.each do |item|
  puts "#{item.name} #{item.version} in #{item.catalogs.to_a.join(", ")}"
end

