#/bin/bash -x

DATE=$(date +"%Y.%m.%d")
# 
retirement_path="/u/files/package-retirement"

retirement_space_before=$(du -s ${retirement_path} | awk '{print $1}')

#get a copy of the all catalog
cp /u/files/packages/catalogs/all $(pwd)

#run munkiclean to pull out packgages not needed /u/munkiclean/
ruby ./munkiclean2.rb > /u/munkiclean/todaysretirement


#compair them against a list that shouldn't be retired
grep -v -f /u/munkiclean/excluded /u/munkiclean/todaysretirement > /u/munkiclean/todaysretirement-${DATE}

pkgcount=$(wc -l /u/munkiclean/todaysretirement-$DATE | awk '{print $1}' | sed 's/ //g')

RETIRE=`cat /u/munkiclean/todaysretirement-$DATE | awk -F':' '{print $3}'`

RETIREINFO=`cat /u/munkiclean/todaysretirement-$DATE | awk -F':' '{print $3}' | sed 's/dmg/plist/g;s/pkg/plist/g'`

echo "Going to retire the following::" >> /u/munkiclean/retired-${DATE}

for i in $RETIRE; do
mv --backup=numbered /u/files/packages/pkgs/$i $retirement_path
echo $i >> /u/munkiclean/retired-$DATE
done

for i in $RETIREINFO; do
mv --backup=numbered /u/files/packages/pkgsinfo/$i $retirement_path
echo $i >> /u/munkiclean/retired-$DATE
done

retirement_space_after=$(du -s /u/files/package-retirement | awk '{print $1}')
spacefree=$(echo $(($retirement_space_after - $retirement_space_before)) | awk '{$1=$1/(1024^2); print $1,"GB"}')

echo "Moved '${pkgcount}' packages to Retirement freeing up '${spacefree}'."

curl -d "{\"color\":\"purple\",\"message\":\"Moved ${pkgcount} packages to Retirement freeing up ${spacefree}.\",\"notify\":false,\"message_format\":\"text\"}" -H 'Content-Type: application/json' https://miworkspace.hipchat.com/v2/room/2373104/notification?auth_token=66IFn0fNGogWbKen96D0lvCCTu26pxuYb2mlf49F

